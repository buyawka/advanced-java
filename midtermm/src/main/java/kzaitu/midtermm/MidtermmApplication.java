package kzaitu.midtermm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MidtermmApplication {

	public static void main(String[] args) {
		SpringApplication.run(MidtermmApplication.class, args);
	}

}
