package kzaitu.midtermm.service;

import kzaitu.midtermm.model.Person;
import kzaitu.midtermm.repository.PersonRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@AllArgsConstructor
public class PersonService {
    private final PersonRepository personRepository;

    public PersonService() {
        personRepository = null;
    }


    public List<Person> getAll() {
        return (List<Person>) personRepository.findAll();
    }

    public Person save(Person person){
        return personRepository.save(person);
    }

    public void deleteById(Long id) {
        personRepository.deleteById(id);
    }

}
