package kzaitu.midtermm.model;

import javax.persistence.*;
@Entity
@Table
public class Person {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String fName;
    private  String lName;
    private String city;
    private long phone;
    private long telegram;

}
