package kzaitu.midtermm.repository;

import kzaitu.midtermm.model.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface PersonRepository extends CrudRepository <Person, Long> {
}
