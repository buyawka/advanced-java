package kzaitu.midtermm.controller;

import kzaitu.midtermm.model.Person;
import kzaitu.midtermm.service.PersonService;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor


public class PersonController {
    private PersonService personService;

    @GetMapping("/api/v2/users/")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(personService.getAll());
    }

    @PostMapping("/api/v2/users/")
    public ResponseEntity<?> createPerson(@RequestBody Person person){
        return ResponseEntity.ok(personService.save(person));
    }
    @DeleteMapping("/api/v2/users/{id}")
    public void deleteById(@PathVariable Long id){
        personService.deleteById(id);
    }

}
